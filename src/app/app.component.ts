import {Component} from '@angular/core';
import {LoadingService} from './services/loading.service';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  public readonly loadingService: LoadingService;

  constructor(loadingService: LoadingService) {
    this.loadingService = loadingService;
  }
}

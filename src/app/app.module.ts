import {NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';

import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {HttpClientModule} from '@angular/common/http';
import {CommonModule} from '@angular/common';
import {PipelineStatusComponent} from './components/pipeline-status/pipeline-status.component';
import {GroupService} from './services/gitlab/group.service';
import {ProjectService} from './services/gitlab/project.service';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {materialModules} from 'src/angular-material-modules';

import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {SettingsComponent} from "./components/settings/settings.component";
import {GroupComponent} from "./components/group/group.component";
import {GroupProjectsComponent} from "./components/group-projects/group-projects.component";
import {ProjectComponent} from "./components/project/project.component";
import {PipelineService} from "./services/gitlab/pipeline.service";
import {NavbarMainComponent} from "./layout/main/navbar/navbar.main.component";
import {HomePageComponent} from "./pages/home.page.component";

const gitlabServices = [
  GroupService,
  ProjectService,
  PipelineService
];

@NgModule({
  declarations: [
    AppComponent,
    PipelineStatusComponent,
    SettingsComponent,
    GroupComponent,
    GroupProjectsComponent,
    ProjectComponent,
    PipelineStatusComponent,
    NavbarMainComponent,
    HomePageComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    BrowserAnimationsModule,
    HttpClientModule,
    CommonModule,

    ...materialModules,
    AppRoutingModule,
  ],
  providers: [
    ...gitlabServices
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}

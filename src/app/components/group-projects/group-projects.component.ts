import {Component, Input, OnInit} from "@angular/core";
import {ProjectService} from "../../services/gitlab/project.service";

@Component({
  selector: 'app-group-projects',
  templateUrl: './group-projects.component.html'
})
export class GroupProjectsComponent implements OnInit {

  @Input()
  public group: any;

  public projects = [];
  private projectService: ProjectService;

  constructor(projectService: ProjectService) {
    this.projectService = projectService;
  }

  async ngOnInit(): Promise<void> {
    this.projects = await this.projectService.getProjectsByGroupId(this.group.id);
  }


}

import {Component, Input} from "@angular/core";

@Component({
  selector: 'app-gitlab-group',
  templateUrl: './group.component.html'
})
export class GroupComponent {

  @Input()
  public group: any;

}

import {Component, Input, OnInit, Pipe} from "@angular/core";
import {PipelineService} from "../../services/gitlab/pipeline.service";



@Component({
    selector: 'app-pipeline-status',
    templateUrl: './pipeline-status.component.html'
})
export class PipelineStatusComponent implements OnInit {
  private readonly pipelineService: PipelineService;

  @Input()
  public project!: any;

  @Input()
  public branch!: string;

  public pipelines: any;

  constructor(pipelineService: PipelineService) {
    this.pipelineService = pipelineService;
  }

  async ngOnInit(): Promise<void> {
    const data = await this.pipelineService.findLatestPipelineByBranch(this.project.id, this.branch);
    console.log(data);
    this.pipelines = data.body;
  }

  getColor(pipeline: any) {
    if(pipeline.status === "success") {
      return "primary";
    } else if(pipeline.status === "manual") {
      return "accent";
    } else if (pipeline.status === "failed") {
      return "warn";
    } else if (pipeline.status === "skipped") {
      return "accent";
    } else {
      return "accent";
    }
  }

}

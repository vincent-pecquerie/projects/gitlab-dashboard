import {Component, Input} from "@angular/core";
import {GITLAB_HOST_KEY} from "../../constants";

@Component({
  selector: 'app-project',
  templateUrl: './project.component.html'
})
export class ProjectComponent {

  @Input()
  public project: any;

  public baseUrl = localStorage.getItem(GITLAB_HOST_KEY);

  goToProject() {

  }
}

import { Component, OnInit } from "@angular/core";
import { MatDialogRef } from '@angular/material/dialog';
import { GITLAB_HOST_KEY, GITLAB_TOKEN_KEY } from "src/app/constants";
import { SettingsForm } from "./settings.form";
@Component({
    templateUrl: './settings.component.html'
})
export class SettingsComponent implements OnInit {

    public form = SettingsForm;
    private readonly dialogRef: MatDialogRef<SettingsComponent>;

    constructor(dialogRef: MatDialogRef<SettingsComponent>) {
        this.dialogRef = dialogRef;
    }
    ngOnInit(): void {
        this.form.setValue({
            token: localStorage.getItem(GITLAB_TOKEN_KEY),
            baseUrl: localStorage.getItem(GITLAB_HOST_KEY)
        });
    }

    close() {
        const {token, baseUrl} = this.form.getRawValue();
        localStorage.setItem(GITLAB_HOST_KEY, baseUrl || '');
        localStorage.setItem(GITLAB_TOKEN_KEY, token || '');
        this.dialogRef.close();
    }

}
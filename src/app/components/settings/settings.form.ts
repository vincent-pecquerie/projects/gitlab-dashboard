import { FormControl, FormGroup } from "@angular/forms";

export const SettingsForm = new FormGroup({
    baseUrl: new FormControl('', []),
    token: new FormControl('', [])
});
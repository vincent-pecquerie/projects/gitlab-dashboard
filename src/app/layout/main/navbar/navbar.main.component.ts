import {Component, OnInit} from "@angular/core";
import {SettingsComponent} from "../../../components/settings/settings.component";
import {MatDialog} from "@angular/material/dialog";
import {GITLAB_SEARCH_KEY} from "../../../constants";

@Component({
  selector: 'app-navbar-main',
  templateUrl: './navbar.main.component.html',
  styleUrls: ['./navbar.main.component.scss']
})
export class NavbarMainComponent implements OnInit {

  public search!: string;

  private readonly dialogService: MatDialog;

  constructor(dialogService: MatDialog) {
    this.dialogService = dialogService;
  }

  ngOnInit(): void {
    this.search = localStorage.getItem(GITLAB_SEARCH_KEY) || "";
  }



  openSettings() {
    const dialogRef = this.dialogService.open(SettingsComponent);
    dialogRef.afterClosed().subscribe(() => window.location.reload());
  }

  doSearch() {
    localStorage.setItem(GITLAB_SEARCH_KEY, this.search);
    window.location.reload();
  }
}

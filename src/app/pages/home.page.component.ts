import {Component, OnInit} from "@angular/core";
import {GITLAB_HOST_KEY, GITLAB_SEARCH_KEY, GITLAB_TOKEN_KEY} from "../constants";
import {LoadingService} from "../services/loading.service";
import {MatDialog} from "@angular/material/dialog";
import {ProjectService} from "../services/gitlab/project.service";
import {SettingsComponent} from "../components/settings/settings.component";
import {PageEvent} from "@angular/material/paginator";
import {MatSnackBar} from "@angular/material/snack-bar";

@Component({
  templateUrl: './home.page.component.html'
})
export class HomePageComponent implements OnInit {


  public page = 1;
  public total = 0;
  public pageSize = 25;

  public baseUrl = localStorage.getItem(GITLAB_HOST_KEY);
  public projects: any;
  public displayedColumns: string[] = ['name', 'DEV', 'UAT', 'PROD'];

  public readonly loadingService: LoadingService;
  private readonly dialogService: MatDialog;
  private readonly projectService: ProjectService;
  private readonly snackBar: MatSnackBar;


  constructor(
    dialogService: MatDialog,
    loadingService: LoadingService,
    projectService: ProjectService,
    snackBar: MatSnackBar
  ) {
    this.dialogService = dialogService;
    this.loadingService = loadingService;
    this.projectService = projectService;
    this.snackBar = snackBar;
  }

  async ngOnInit(): Promise<void> {

    if (localStorage.getItem(GITLAB_HOST_KEY) == null || localStorage.getItem(GITLAB_TOKEN_KEY) == null) {
      const dialogRef = this.dialogService.open(SettingsComponent);
      dialogRef.afterClosed().subscribe(() => window.location.reload());
    } else {
      await this.loadPage(1);
    }
  }

  async loadPage(page: number) {

    const search = localStorage.getItem(GITLAB_SEARCH_KEY) || "";

    this.loadingService.loading = true;
    this.loadingService.loadingMessage = "Chargement des projets en cours...";
    const data = await this.projectService
      .searchProjectByPage(search, page, this.pageSize)
      .catch(() => {
        this.snackBar.open("Impossible d'obtenir les projets.");
        this.loadingService.loadingMessage = "";
        this.loadingService.loading = false;
      });
    this.total = data.headers.get('x-total');
    this.projects = data.body;
    this.loadingService.loading = false;
  }

  async changePage($event: PageEvent) {
    this.pageSize = $event.pageSize;
    await this.loadPage($event.pageIndex);
  }


}

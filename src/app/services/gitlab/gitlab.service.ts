import {HttpClient} from "@angular/common/http";
import {firstValueFrom} from "rxjs/internal/firstValueFrom";
import {GITLAB_HOST_KEY, GITLAB_TOKEN_KEY} from "src/app/constants";
import {Observable} from "rxjs";

export class GitlabService {

  protected readonly http: HttpClient;

  constructor(http: HttpClient) {
    this.http = http;
  }

  protected getToken(): string | null {
    return localStorage.getItem(GITLAB_TOKEN_KEY);
  }

  protected getBaseUrl(): string | null {
    return localStorage.getItem(GITLAB_HOST_KEY);
  }

  protected getHeaders(): any {
    return {
      "PRIVATE-TOKEN": this.getToken()
    }
  }

  public head(path: String): Observable<any> {
    const url = `${this.getBaseUrl()}${path}`;
    return this.http.head(url, {
      headers: this.getHeaders()
    });

  }


  public async get(path: String): Promise<any> {
    const url = `${this.getBaseUrl()}${path}`;
    return await firstValueFrom(
      this.http.get(url, {
        headers: this.getHeaders(),
        observe: 'response'
      })
    );
  }

  public async post(path: String, body: any): Promise<any> {
    const url = `${this.getBaseUrl()}${path}`;
    return await firstValueFrom(
      this.http.post(url, body, {
        headers: this.getHeaders()
      })
    );
  }

  public async put(path: String, body: any): Promise<any> {
    const url = `${this.getBaseUrl()}${path}`;
    return await firstValueFrom(
      this.http.put(url, body, {
        headers: this.getHeaders()
      })
    );
  }

  public async delete(path: String): Promise<any> {
    const url = `${this.getBaseUrl()}${path}`;
    return await firstValueFrom(
      this.http.delete(url, {
        headers: this.getHeaders()
      })
    );
  }

}

import { HttpClient } from "@angular/common/http";
import { GitlabService } from "./gitlab.service";
import { Injectable } from "@angular/core";

@Injectable()
export class GroupService extends GitlabService {
    constructor(http: HttpClient) {
        super(http);
    }

    async getGroups() {
        const path = `/api/v4/groups?per_page=500`;
        return this.get(path);
    }

    async getSubgroups(groupId: number) {
        const path = `/api/v4/groups/${groupId}/subgroups?per_page=500`;
        return this.get(path);
    }
}
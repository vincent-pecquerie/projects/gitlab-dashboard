import {Injectable} from "@angular/core";
import {GitlabService} from "./gitlab.service";
import {HttpClient} from "@angular/common/http";

@Injectable()
export class PipelineService extends GitlabService {

  constructor(http: HttpClient) {
    super(http);
  }

  public async findLatestPipelineByBranch(projectId: number, branch: string) {
    const path = `/api/v4/projects/${projectId}/pipelines?ref=${branch}&per_page=1`;
    return await this.get(path);
  }
}

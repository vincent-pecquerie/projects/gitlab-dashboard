import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { firstValueFrom } from "rxjs/internal/firstValueFrom";
import { GitlabService } from "./gitlab.service";

@Injectable()
export class ProjectService extends GitlabService {

    constructor(http: HttpClient) {
        super(http);
    }


    async searchProjectByPage(search: string, page: number, numberPerPage: number) {
      const path = `/api/v4/projects?archived=false&order_by=last_activity_at&per_page=${numberPerPage}&page=${page}&search=${search}`;
      return await this.get(path);
    }

    async getProjectsByGroupId(groupId: number) {
        const path = `/api/v4/groups/${groupId}/projects?per_page=500`;
        return await this.get(path);
    }

}

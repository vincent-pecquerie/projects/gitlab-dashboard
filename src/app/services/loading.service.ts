import { Injectable } from "@angular/core";

@Injectable({ providedIn: 'root' })
export class LoadingService {
    public loading: boolean = true;
    public loadingMessage: string = "";
}